//http://js.do/Davind3r/payday_readable
//v1.0 2016-09-26 07:03pm

//TODO: Maybe change alert to an over-laid html div?


//Output:
//	The Paydate for this month might be dd-mmm-yyyy dddd	//Second Last working day of month. (Out of Sun,Mon,Tue,Wed,Thu)
//	Days Remaining from today #

// Get today's date from system, get day (1-31), Day (Sunday....), Month(1-12), Year YYYY from it
var cdate = new Date();
var cday = cdate.getDate();
var cwday = cdate.getDay();
var cmonth = cdate.getMonth();
var cyear = cdate.getFullYear();

//if today's date is 30 or less, add one to today's month.
// reason because you might already have got paid, & nopw you want to know your next payday you greedy
if (cday>=30){
	cmonth = cmonth+1;}

//set paydate to last date of this month
var paydate = new Date(cyear, cmonth+1, 0);

//now start looking if this last day is working or not?
//working days are Sun-Thu
//check 7 times, because then days will repeat
//if not minus one from the date, & rerun the loop
//break loop when day is Sun-Thu
for(i = 0; i < 7; i++){
var paywday = paydate.getDay();
	if(paywday > 4){
		paydate.setDate(paydate.getDate()-1);
	   } else {
	   break;
	   }
}

//prepare the output string
// load day full names
var weekday = new Array(5);
weekday[0]=  "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";

//get day Full name from above array
//get date (1-31)
//get month (1-12)
var payday = weekday[paydate.getDay()];
var paydateday = paydate.getDate(); 
var paydatemonth = paydate.getMonth()+1; 
var paydateyear = paydate.getFullYear();

var seperator = "-"; //date seperater
var paydatestring = paydateday + seperator + paydatemonth + seperator + paydateyear + " " + payday + ".";

//calculate days remaing from today
//convert today & paydate to UNIX time in milliseconds, divide the difference by milliseconds in one day, 24 Hours x 3600 Seconds x 1000 Milliseconds
var daysdiff = Math.ceil((Math.abs(paydate.getTime()-cdate.getTime()))/(1000*3600*24))-1;

//send an alert with all this string
alert("The Paydate for this month might be " + paydatestring + "\n\n Days Remaining from today: " + daysdiff);