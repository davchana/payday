# payday
A JavaScript bookmarklet to get the next pay day for Staff.
Although this code is public, it is of use to only specific employees. Please use it at your own risk.

## Bookmarklet Code

Same as in payday_vX_03_bookmarlet.js _(double or triple click this below block for selecting all text)_

```javascript
javascript:(function()%7Bvar%20c%3Dnew%20Date%2Cb%3Dc.getDate()%3Bc.getDay()%3Bvar%20a%3Dc.getMonth()%2Cd%3Dc.getFullYear()%3B30%3C%3Db%26%26(a%2B%3D1)%3Bb%3Dnew%20Date(d%2Ca%2B1%2C0)%3Bfor(i%3D0%3B7%3Ei%3Bi%2B%2B)if(4%3Cb.getDay())b.setDate(b.getDate()-1)%3Belse%20break%3Ba%3DArray(5)%3Ba%5B0%5D%3D%22Sunday%22%3Ba%5B1%5D%3D%22Monday%22%3Ba%5B2%5D%3D%22Tuesday%22%3Ba%5B3%5D%3D%22Wednesday%22%3Ba%5B4%5D%3D%22Thursday%22%3Bvar%20a%3Da%5Bb.getDay()%5D%2Cd%3Db.getDate()%2Ce%3Db.getMonth()%2B1%2Cf%3Db.getFullYear()%2Ca%3Dd%2B%22-%22%2Be%2B%22-%22%2Bf%2B%22%20%22%2Ba%2B%22.%22%2Cc%3DMath.ceil(Math.abs(b.getTime()-c.getTime())%2F864E5)-1%3Balert(%22The%20Paydate%20for%20this%20month%20might%20be%20%22%2Ba%2B%22%5Cn%5Cn%20Days%20Remaining%20from%20today%3A%20%22%2Bc)%7D)())
```

## Usage

1. Copy the javascript code from [payday_03_bookmarlet](https://gitlab.com/davchana/payday/blob/beta/payday_v1_03_bookmarklet.js) _(only the code, not comment)_ or from above block.
1. Right click on Chrome's bookmark bar > Add page; or three dot menu > Bookmarks > Manager > Add page
1. Type PayDay as Name/Title
2. Paste the code copied earlier in URL field.


## ToDo

* ~~To upload the code files from local.~~ Done 2016-09-21 07:21pm
* ~~To document the three different versions of this code.~~ Done 2016-09-21 07:21pm
  * ~~First: The fully documented & indented javascript code.~~ Yes, Done, see file 01
  * ~~Second: The minified & compressed version of above.~~ Yes, Done see file 02
  * ~~Third: Above in boomarklet form, ready to be used as bookmarklet.~~ Yes, Done, see file 03
* ~~Move this TODO to Issues Tab (all new TODOs).~~ Done, 2016-09-21 07:21pm

_Put all new TODOs as Issues_
